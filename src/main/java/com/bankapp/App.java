package com.bankapp;

import com.bankapp.user.*;

import java.util.*;

import static com.bankapp.user.AccountType.CARD;
import static com.bankapp.user.AccountType.SAVING;
import static com.bankapp.user.AccountType.CREDIT;
import static com.bankapp.user.AccountType.DEPOSIT;

public class App {
    public static void main(String[] args) {
        UserService us = new UserService();
        AccountService as = new AccountService();
        AccountServiceOperations aso = new AccountServiceOperations();
        ArrayList<User> users = new ArrayList<>();

        User user1 = us.createUser();
        User user2 = us.createUser();

        users.add(user1);
        users.add(user2);

        aso.addMoney(user1, SAVING, 500.0);
        aso.withdrawMoney(user1, SAVING, 100.0);

        for (User user : users) {
            System.out.println("Пользователь: " + user.getName() + " " + user.getSurname());
            System.out.println("Email: " + user.getEmail());
            System.out.println("Телефон(ы): ");
            System.out.println(user.getPhone().getTelWork() + " -- рабочий телефон");
            System.out.println(user.getPhone().getTelHome() + " -- домашний телефон");
            System.out.println("Счета:");
            System.out.println(user.getAccounts());
            System.out.println("---------------------");
            System.out.println();
        }
    }
}
