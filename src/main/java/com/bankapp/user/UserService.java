package com.bankapp.user;

import com.bankapp.user.*;

import java.util.*;

import static com.bankapp.user.AccountType.*;
import static com.bankapp.user.AccountType.DEPOSIT;

public class UserService {
    public User createUser() {
        String name = null;
        String surname = null;
        String email = null;
        int telWork = 0;
        int telHome = 0;

        AccountService as = new AccountService();

        System.out.println("Новый пользователь.");
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите имя:");
        if (scanner.hasNextLine()) {
            name = scanner.nextLine();
        }
        System.out.println("Введите фамилию:");
        if (scanner.hasNextLine()) {
            surname = scanner.nextLine();
        }
        System.out.println("Введите e-mail:");
        if (scanner.hasNextLine()) {
            email = scanner.nextLine();
        }
        System.out.println("Введите рабочий телефон:");
        if (scanner.hasNextInt()) {
            telWork = scanner.nextInt();
        }
        System.out.println("Введите домашний телефон:");
        if (scanner.hasNextInt()) {
            telHome = scanner.nextInt();
        }

        User user = new User(name, surname, email, new Telephone(telWork, telHome));
        as.addAccountToUser(user, SAVING);
        as.addAccountToUser(user, CARD);
        as.addAccountToUser(user, CREDIT);
        as.addAccountToUser(user, DEPOSIT);
        return user;
    }

    public void blockUser(User user) {
        user.ban = true;
    }

    public void deleteUser(User user) {
        user = null;
    }
}
