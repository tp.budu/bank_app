package com.bankapp.user;

import com.bankapp.user.*;

public class AccountService {
    public void addAccountToUser(User user, AccountType accType) {
        user.accounts.add(new Account(accType, genNumber()));
    }

    //генерируем случайное число для примера
    private String genNumber() {
        String num = "";
        for (int i = 0; i < 11; i++) {
            num += (int) (Math.random() * 10);
        }
        return num;
    }
}
