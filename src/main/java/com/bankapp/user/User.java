package com.bankapp.user;


import java.util.*;

public class User {
    private static int id = 1;
    private String name;
    private String surname;
    private String email;
    private Telephone phone;
    protected ArrayList<Account> accounts = new ArrayList<>();
    protected boolean ban = false;

    public User(String name, String surname, String email, Telephone phone) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        id++;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public Telephone getPhone() {
        return phone;
    }

    public String getAccounts() {
        if (accounts != null) {
            StringBuilder str = new StringBuilder();
            for (Account account : accounts) {
                str.append(account.getAccTypeName()
                        + " - " + account.getAccountNumber()
                        + " - " + account.getCount() + " денег на счету" + "\n");
            }
            return str.toString();
        } else {
            return "У пользователя нет счетов.";
        }
    }
}


