package com.bankapp.user;

import com.bankapp.user.*;

public class AccountServiceOperations {
    public void addMoney(User user, AccountType accType, double addedSum) {
        double current = 0.0;

        for (Account account : user.accounts) {
            if (account.accType.equals(accType)) {
                current = account.getCount();
                account.setCount(current + addedSum);
                break;
            }
        }
    }

    public void withdrawMoney(User user, AccountType accType, double withdrawSum) {
        double current = 0.0;

        for (Account account : user.accounts) {
            if (account.accType.equals(accType)) {
                current = account.getCount();
                account.setCount(current - withdrawSum);
                break;
            }
        }
    }
}
