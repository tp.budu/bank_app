package com.bankapp.user;

public class Telephone {
    private int telWork;
    private int telHome;

    public Telephone(int telWork, int telHome) {
        this.telWork = telWork;
        this.telHome = telHome;
    }

    public int getTelWork() {
        return telWork;
    }

    public int getTelHome() {
        return telHome;
    }
}
