package com.bankapp.user;

public class Account {
    protected AccountType accType;
    private String accountNumber;
    private double count = 0;

    public Account(AccountType accType, String accountNumber) {
        this.accType = accType;
        this.accountNumber = accountNumber;
    }

    //получает сумму средств на счете
    public double getCount() {
        return count;
    }

    //устанавливает сумму средств на счете
    public void setCount(double count) {
        this.count = count;
    }

    //название счета (тип)
    public String getAccTypeName() {
        return accType.getTypeName();
    }

    //номер счета
    public String getAccountNumber() {
        return accountNumber;
    }
}
