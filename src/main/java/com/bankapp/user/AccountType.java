package com.bankapp.user;

public enum AccountType {
    SAVING("Сберегательный счет"),
    CARD("Карточный счет"),
    CREDIT("Кредитный счет"),
    DEPOSIT("Депозитный счет");

    private String typeName;

    AccountType(String str) {
        typeName = str;
    }

    public String getTypeName() {
        return typeName;
    }
}
